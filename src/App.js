import React, { useEffect, useReducer, useState } from 'react'
import { ipcRenderer } from 'electron';
import LogItem from './components/LogItem';
import Alert from './components/Alert';
import AddLogItem from './components/AddLogItem';
import alertReducer from './components/Alert/AlertReducer';
import { Container, Table, TableBody, TableHead, TableContainer, Paper, TableRow, TableCell, Stack, CircularProgress, Grid } from '@mui/material'


const App = (props) => {
  const [logs, setLogs] = useState(props.logs || [])
  const [loading, setLoading] = useState(props.loading === undefined ? true : false)
  const [alertState, dispatch] = useReducer(alertReducer, {})

  useEffect(() => {
    // send connection to mainWindow to get DB logs
    ipcRenderer.send("logs:load")

    // listen for returned logs
    ipcRenderer.on("logs:get", (e, logs) => {
      setLogs(JSON.parse(logs))
      setLoading(false)
    })
  }, [])
  
  return (
    <Container style={{ padding: 15 }} title="app">
      <Stack spacing={2}>
        <AddLogItem dispatch={dispatch} />
        {loading ? (
          <Grid container alignContent="center" justifyContent="center" title="loading-circle">
            <Grid item>
              <CircularProgress />
            </Grid>
          </Grid>
        ) : (
          <TableContainer component={Paper} title="log-table">
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell align="center">Priority</TableCell>
                  <TableCell align="center">Log Text</TableCell>
                  <TableCell align="center">User</TableCell>
                  <TableCell align="center">Created at</TableCell>
                  <TableCell align="center">Delete Log</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {logs.length === 0 ? (
                  <TableRow>
                    <TableCell>
                      No logs currently.
                    </TableCell>
                  </TableRow>
                ) : null}
                {logs.map((bug, idx) => (
                  <LogItem bug={bug} key={bug._id || idx} dispatch={dispatch} />
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        )}
      </Stack>
      <Alert alertState={alertState} dispatch={dispatch} />
    </Container>
  )
}

export default App
