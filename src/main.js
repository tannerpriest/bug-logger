import path from "path"
import Log from "../models/Log"
import connectDB from "../config/db"
import { app, BrowserWindow, ipcMain } from "electron"

const returnError = (err) => JSON.stringify({ type: "error", payload: err })
const returnAction = (type) => JSON.stringify({ type })

connectDB()

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) {
  // eslint-disable-line global-require
  app.quit();
}

let isDev = false

if (
  process.env.NODE_ENV !== undefined &&
  process.env.NODE_ENV === "development"
) {
  isDev = true
}

/**
 * @type {BrowserWindow}
*/

let mainWindow;

const createWindow = () => {
  mainWindow = new BrowserWindow({
    width: 1100,
    height: 800,
    icon: "./assets/icon.png",
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
      devTools: isDev
    },
  });

  mainWindow.loadURL(MAIN_WINDOW_WEBPACK_ENTRY);

  if (isDev) mainWindow.webContents.openDevTools();

};

app.on('ready', createWindow);

const sendLogs = async () => {
  const logs = await Log.find().sort({ created: 1 })
    .catch(err => {
      console.error("Error while getting logs: ", err)
      mainWindow.webContents.send("logs:error", err)
    })
  mainWindow.webContents.send("logs:get", JSON.stringify(logs))
}

ipcMain.on("logs:load", sendLogs)

ipcMain.on("logs:delete", async (e, _id) => {
  await Log.deleteOne({ _id }).then(() => {
    e.returnValue = returnAction("delete")
    sendLogs()
  }).catch((err) => {
    console.error("Error while deleting Log: ", err)
    e.returnValue = returnError(err)
  })
})

ipcMain.on("logs:add", async (e, newLog) => {
  await Log.create(newLog)
    .then(() => {
      e.returnValue = returnAction("new")
      sendLogs()
    })
    .catch(err => {
      console.error("Error while creating Log: ", err)
      e.returnValue = returnError(err)
    })
})

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});


