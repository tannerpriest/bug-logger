import { CssBaseline } from '@mui/material';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';

function render() {
  ReactDOM.render(<>
  <CssBaseline />
    <App />
  </>, document.getElementById('root'));
}

render();