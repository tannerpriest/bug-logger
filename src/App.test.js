import React, { useState } from "react"
import App from "./App";
import { render } from "@testing-library/react";
import { queryByTitleAttr } from "./test/testUtils";

jest.mock("electron", () => ({
    // @ts-ignore
    ...jest.requireActual('electron'),
    ipcRenderer: {
        send: jest.fn(),
        on: jest.fn(),
    }
}))

const setup = (initalState = {}) => {
    return render(<App {...initalState} />)
}

describe('render', () => {
    let wrapper;
    beforeEach(() => {
        wrapper = setup()
    })
    test('should render without error', () => {
        const component = queryByTitleAttr(wrapper, "app")
        expect(component).toBeTruthy()
    })
    test('loading circle', () => {
        const loadingCircle = queryByTitleAttr(wrapper, "loading-circle")
        expect(loadingCircle).toBeTruthy()
    })
})

describe("if loading is false", () => {
    let wrapper;
    beforeEach(() => {
        wrapper = setup({ loading: false })
    })
    test('loading circle should not render', () => {
        const loadingCircle = queryByTitleAttr(wrapper, "loading-circle")
        expect(loadingCircle).toBeFalsy()
    })
    test('log table should render', () => {
        const logTable = queryByTitleAttr(wrapper, "log-table")
        expect(logTable).toBeTruthy()
    })
})
