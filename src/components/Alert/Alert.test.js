import React from "react";
import Alert from "./Alert";
import { render, act, waitFor } from "@testing-library/react";
import { queryByTitleAttr } from "../../test/testUtils"
import userEvent from "@testing-library/user-event";

jest.mock("electron", () => ({
    // @ts-ignore
    ...jest.requireActual('electron'),
    ipcRenderer: {
        on: jest.fn()
    }
}))

const defaultState = {
    alertState: {
        message: "asdf",
        severity: "info",
    },
    dispatch: () => { }
}


const setup = (initalState = defaultState) => {
    return render(<Alert {...initalState} />)
}


describe("render", () => {
    let wrapper;

    beforeEach(() => {
        wrapper = setup()
        jest.clearAllMocks()
    })

    afterEach(() => {
        wrapper = null
    })

    test("should render with message", () => {
        let component = queryByTitleAttr(wrapper, "alert-component")
        expect(component).toBeTruthy()
    })
})

describe("should not render", () => {
    test("on click, should close snackbar", async () => {
        jest.clearAllMocks()

        let wrapper = setup()

        let alertSnackBar = queryByTitleAttr(wrapper, "alert-component")

        // double check to see if snackbar has rendered
        expect(alertSnackBar).toBeTruthy()

        // onclick should close snackbar
        userEvent.click(alertSnackBar);

        await waitFor(() => {
            let alertComp = queryByTitleAttr(wrapper, "alert-component")
            expect(alertComp).toBeFalsy()
        })
    })
})

describe('should show correct messages', () => {
    let wrapper;
    let alertMessage = "Testing123"
    let stateHolder = {...defaultState}
    beforeEach(() => {
        stateHolder.alertState.message = alertMessage
        stateHolder.alertState.severity = "warning"
        wrapper = setup(stateHolder)
    })

    afterEach(() => {
        jest.clearAllMocks()
    })

    test("should show alertMessage",() => {
        let component = queryByTitleAttr(wrapper, "alert-message")
        expect(component).toBeTruthy()
        expect(component.lastChild.textContent).toBe(alertMessage)
    })
    test("should show alertMessage",() => {
        let component = queryByTitleAttr(wrapper, "alert-message")
        expect(component).toBeTruthy()
        expect(component.className.includes("MuiAlert-filledWarning")).toBe(true)
    })
})

