const alertReducer = (state, action) => {
    switch (action.type) {
        case 'new':
            return {
                message: "New Log was created",
                severity: "success"
            };
        case 'delete':
            return {
                message: "Log was deleted",
                severity: "info"
            };
        case 'notValid':
            return {
                message: "You cannot add a log without a user or log text.",
                severity: "warning"
            };
        case 'clear':
            return {}
        case "error":
            return {
                message: action.payload,
                severity: "error"
            }
        default:
            throw new Error();
    }
}

export default alertReducer