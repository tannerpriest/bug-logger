import React, { useEffect, useState } from 'react'
import { Alert as MaterialAlert, Snackbar } from '@mui/material'
import { ipcRenderer } from 'electron'

const Alert = ({ alertState, dispatch }) => {
    const [open, setOpen] = useState(false)

    const handleClose = () => {
        setOpen(false)
        dispatch({ type: "clear" })
    }

    useEffect(() => {
        ipcRenderer.on("logs:error", (e, err) => {
            dispatch({ type: "error", payload: err })
        })
    }, [dispatch])

    useEffect(() => {
        if (alertState.message) {
            setOpen(true)
        }
    }, [alertState.message])

    return (
        <Snackbar
            open={open}
            autoHideDuration={3000}
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'center',
            }}
            onClose={handleClose}
            title="alert-component"
            onClick={handleClose}
        >
            {open ?
                <MaterialAlert
                    variant="filled"
                    title="alert-message"
                    severity={alertState?.severity}
                >
                    {alertState.message}
                </MaterialAlert>
                : null
            }
        </Snackbar>
    )
}

export default Alert