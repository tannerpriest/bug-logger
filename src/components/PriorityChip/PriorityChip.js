// @ts-nocheck
import React from 'react'
import { Chip } from '@mui/material'

const PriorityChip = ({ priority }) => {
    const handlePriority = () => {
        switch (priority) {
            case "high":
                return {
                    color: "error",
                    label: "High"
                }
            case "moderate":
                return {
                    color: "warning",
                    label: "Moderate"
                }
            default:
                return {
                    color: "info",
                    label: "Low"
                }
        }
    }
    return (
        <Chip title="priority-chip-component" {...handlePriority()} />
    )
}

export default PriorityChip