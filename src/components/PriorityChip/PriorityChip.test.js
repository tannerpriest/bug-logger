import React from "react";
import PriorityChip from "./PriorityChip";
import { render } from "@testing-library/react";
import { queryByTitleAttr } from "../../test/testUtils";


const setup = (initalState = {}) => {
    return render(<PriorityChip priority="low" {...initalState} />)
}

const severity = ["low", "moderate", "high"]

describe("render", () => {
    let wrapper
    let count = 0;

    beforeEach(() => {
        wrapper = setup({ priority: severity[count] })
        count++
    })
    
    test(`render chip with default prop "low"`, () => {
        const component = queryByTitleAttr(wrapper, "priority-chip-component")
        expect(component.classList.contains("MuiChip-colorInfo")).toBeTruthy()
    })
    
    test(`render chip with default prop "moderate"`, () => {
        const component = queryByTitleAttr(wrapper, "priority-chip-component")
        expect(component.classList.contains("MuiChip-colorWarning")).toBeTruthy()
    })

    test(`render chip with default prop "high"`, () => {
        const component = queryByTitleAttr(wrapper, "priority-chip-component")
        expect(component.classList.contains("MuiChip-colorError")).toBeTruthy()
    })
})

describe("label content", () => {
    let wrapper
    let count = 0;
    
    beforeEach(() => {
        wrapper = setup({ priority: severity[count] })
        count++
    })
    
    test(`render chip with default prop "low"`, () => {
        const component = queryByTitleAttr(wrapper, "priority-chip-component")
        expect(component.lastChild.textContent).toBe("Low")
    })
    
    test(`render chip with default prop "moderate"`, () => {
        const component = queryByTitleAttr(wrapper, "priority-chip-component")
        expect(component.lastChild.textContent).toBe("Moderate")
    })
    
    test(`render chip with default prop "high"`, () => {
        const component = queryByTitleAttr(wrapper, "priority-chip-component")
        expect(component.lastChild.textContent).toBe("High")
    })

})
