import React from 'react'
import Moment from 'react-moment'
import { ipcRenderer } from 'electron'
import PriorityChip from '../PriorityChip'
import { DeleteForever } from '@mui/icons-material'
import { TableRow, TableCell, IconButton } from '@mui/material'

const LogItem = ({ bug, dispatch }) => {
    const { priority, text, user, created, _id } = bug

    const handleDelete = (e) => {
        e.stopPropagation()
        const res = ipcRenderer.sendSync("logs:delete", _id)
        dispatch(JSON.parse(res))
    }

    return (
        <TableRow title="log-item-component">
            <TableCell align='left'>
                <PriorityChip priority={priority} />
            </TableCell>
            <TableCell align='center' title="log-title">
                {text}
            </TableCell>
            <TableCell align='center' title="log-user">
                {user}
            </TableCell>
            <TableCell align='center' title="log-created-date">
                <Moment format="MMMM Do YYYY, h:mm A">{created}</Moment>
            </TableCell>
            <TableCell align='center'>
                <IconButton color="warning" onClick={handleDelete} title="delete-log-button">
                    <DeleteForever />
                </IconButton>
            </TableCell>
        </TableRow>
    )
}

export default LogItem