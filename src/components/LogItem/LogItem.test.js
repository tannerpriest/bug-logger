import React from "react";
import LogItem from "./LogItem";
import electron from "electron"
import userEvent from "@testing-library/user-event"
import { render } from "@testing-library/react";
import { queryByTitleAttr } from "../../test/testUtils"
import { Table, TableBody } from "@mui/material";
import moment from "moment"

jest.mock("electron", () => ({
    // @ts-ignore
    ...jest.requireActual('electron'),
    ipcRenderer: {
        sendSync: jest.fn(),
    }
}))

const defaultStateBug = {
    _id: "0000001",
    user: "tanner",
    priority: "low",
    text: "testing bug",
    created: Date.now(),
}

const defaultState = {
    bug: defaultStateBug,
    dispatch: () => { },
}

const setup = (initalState = defaultState) => {
    return render(
        <Table>
            <TableBody>
                <LogItem {...initalState} />
            </TableBody>
        </Table>
    )
}


describe('render', () => {
    let wrapper;
    beforeEach(() => {
        wrapper = setup()
    })
    test('should render table row', async () => {
        const component = queryByTitleAttr(wrapper, "log-item-component")
        expect(component).toBeTruthy()
    })
    test('should show created date', async () => {
        const component = queryByTitleAttr(wrapper, "log-created-date")
        expect(component).toBeTruthy()
    })
})

describe('should render props in correct spots', () => {
    // Priority chip tests will cover if it renders the correct props or not

    let wrapper;
    beforeEach(() => {
        wrapper = setup()
    })

    test('should show title', () => {
        let titleCell = queryByTitleAttr(wrapper, "log-title").innerHTML
        expect(titleCell === defaultStateBug.text).toBe(true)
    })
    test('should show user', () => {
        let userCell = queryByTitleAttr(wrapper, "log-user").innerHTML
        expect(userCell === defaultStateBug.user).toBe(true)
    })
    test('should show date', () => {
        let dateCell = queryByTitleAttr(wrapper, "log-created-date");
        let dateContent = dateCell.lastChild.textContent;
        let formattedDate = moment(defaultStateBug.created).format("MMMM Do YYYY, h:mm A");

        // inner text should match the current date stamp in Moment component
        expect(dateContent === formattedDate).toBe(true)
    })

})

describe('delete should work', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = setup()
        jest.clearAllMocks()
    })

    test('should run hook to delete', async () => {

        const spy = jest.spyOn(electron.ipcRenderer, "sendSync").mockImplementation(() => JSON.stringify({ test: 123 }));

        const deleteButton = queryByTitleAttr(wrapper, "delete-log-button")
        expect(deleteButton).toBeTruthy()

        userEvent.click(deleteButton)

        expect(spy).toHaveBeenCalled();
        expect(spy).toHaveBeenCalledWith("logs:delete", defaultStateBug._id);

        spy.mockReset();
        spy.mockRestore();
    })
})
