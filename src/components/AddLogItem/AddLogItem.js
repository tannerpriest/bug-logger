import React, { useState } from 'react'
import { ipcRenderer } from 'electron'
import { Add } from '@mui/icons-material'
import PriorityChip from '../PriorityChip'
import { Select, MenuItem, Card, CardContent, FormControl, InputLabel, TextField, Grid, Button } from '@mui/material'

const defaultValue = {
    text: "",
    user: "",
    priority: "low",
}

const AddLogItem = ({ dispatch }) => {
    const [newLog, setNewLog] = useState(defaultValue)

    const handleChange = (event) => {
        event.preventDefault()
        if (event.target.value === newLog[event.target.name]) {
            return
        }
        setNewLog({
            ...newLog,
            [event.target.name]: event.target.value
        })
    }

    const handleAddLog = (event) => {
        event.preventDefault()
        if (newLog.text === "" || newLog.user === "") {
            dispatch({ type: "notValid" })
            return
        }
        const res = JSON.parse(ipcRenderer.sendSync("logs:add", newLog))
        dispatch(res)

        setNewLog(defaultValue)
    }

    return (
        <Card elevation={3}>
            <CardContent>
                <form onSubmit={handleAddLog}>
                    <Grid container spacing={2} alignItems="center">
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                name='text'
                                placeholder='Log'
                                value={newLog.text}
                                onChange={handleChange}
                            />
                        </Grid>
                        <Grid item maxWidth={300} xs={6}>
                            <TextField
                                fullWidth
                                name='user'
                                placeholder='User'
                                value={newLog.user}
                                onChange={handleChange}
                            />
                        </Grid>
                        <Grid item maxWidth={300} xs={6}>
                            <FormControl fullWidth>
                                <InputLabel >Priority</InputLabel>
                                <Select
                                    label="Priority"
                                    name="priority"
                                    value={newLog.priority}
                                    onChange={handleChange}
                                >
                                    <MenuItem value="low">
                                        <PriorityChip priority="low" />
                                    </MenuItem>
                                    <MenuItem value="moderate">
                                        <PriorityChip priority="moderate" />
                                    </MenuItem>
                                    <MenuItem value="high">
                                        <PriorityChip priority="high" />
                                    </MenuItem>
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item xs={12}>
                            <Button
                                fullWidth
                                type="submit"
                                color="success"
                                variant='contained'
                            >
                                Add Bug <Add />
                            </Button>
                        </Grid>
                    </Grid>
                </form>
            </CardContent>
        </Card>
    )
}

export default AddLogItem