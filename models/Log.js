import mongoose from "mongoose";

const LogSchema = new mongoose.Schema({
    text: {
        type: String,
        trim: true,
        required: [true, "Log text required"]
    },
    priority: {
        type: String,
        default: "low",
        enum: ["low", "moderate", "high"]
    },
    user: {
        type: String,
        trim: true,
        required: [true, "Log user required"]
    },
    created: {
        type: Date,
        default: Date.now,
        required: [true, "Date required"],
    },
})

export default mongoose.model("Log", LogSchema)